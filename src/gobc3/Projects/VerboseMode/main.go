package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Verbose Mode
//
//  When the player runs the game like this:
//
//    go run main.go -v 4
//
//  Display each generated random number:

//    1 3 4 🎉  YOU WIN!
//
//  In this example, computer picks 1, 3, and 4. And the
//  player wins.
//
// HINT
//  You need to get and interpret the command-line arguments.
// ---------------------------------------------------------

const (
	maxTurns = 5
	usage    = `
Welcome to the lucky number game!

The program will pick %d random numbers.
Your mission is to guess one of those numbers.
You have options to enter display computer random numbers and your guessing numbers.

The greater your numbers is the harder it gets.
Enter commnad with option -v will display all data 
Enter command without option -v will only display Win/Loss results

Example: command [-v] [5] enter

`
)

func main() {
	var (
		guess int
		err   error
		v     bool
	)
	// Set random seed number
	rand.Seed(time.Now().UnixNano())

	// Read command-line arguments
	args := os.Args[1:]

	// Test all aurgument for valid input
	if len(args) < 1 {
		fmt.Printf(usage, maxTurns)
		return
	}

	if len(args) == 1 {
		// Convert command options
		guess, err = strconv.Atoi(args[0])
		// Verify err
		if err != nil {
			fmt.Println("Input is not a number.")
			return
		}
		// fmt.Println(guess)
		// Verify correct guess number
		if guess < 0 {
			fmt.Println("Please enter a positive number.")
			return
		}
	} else if len(args) == 2 {
		guess, err = strconv.Atoi(args[1])
		// Verify err
		if err != nil {
			fmt.Println("Input is not a number.")
			return
		}
		// Verity for correct -v option
		if args[0] == "-v" && guess > 0 {
			v = true
		} else {
			fmt.Println("Please enter correct option. Example: command [-v] [5].")
			return
		}
	}

	// Generate Random numbers
	for turn := 1; turn < maxTurns; turn++ {
		n := rand.Intn(guess + 1)
		if v {
			fmt.Println(turn, n, guess)
		}
		if n == guess {
			fmt.Println("🏆 YOU WIN!")
			return
		}
	}

	// Print results
	fmt.Println("☠️  YOU LOSS!")

}
