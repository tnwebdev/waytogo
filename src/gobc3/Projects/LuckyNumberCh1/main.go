// Lucky Number Game Challenge 1

package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	maxTurn = 5
	usage   = `
	Welcome to the Lucky Number Game!
	The program will pick %d random numbers.
	Your mission is to guess one of those numbers.
	
	The greater your numbers is, the harder it gets.
	
	Let play?
	`
)

func main() {
	var (
		msg string
	)

	rand.Seed(time.Now().UnixNano())

	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Printf(usage+"\n", maxTurn)
		return
	}

	guess, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("Not a number.")
		return
	} else if guess < 0 {
		fmt.Println("Please enter a positive number")
		return
	}

	for turn := 1; turn <= maxTurn; turn++ {
		n := rand.Intn(guess + 1)
		fmt.Print(turn)
		fmt.Print(" ", n, " ")

		switch {
		case turn == 1 && n == guess:
			msg = "🏆 CONGRATULATION - You hit the first lucky number!"
		case turn == 2 && n == guess:
			msg = "🏆 EXCELLENT - You won on the second trial!"
		case turn == 3 && n == guess:
			msg = "🏆 WELL DONE: You beat it on the third trial!"
		case turn == 4 && n == guess:
			msg = "🏆 VERY GOOD - You hit the fourth lucky number!"
		case turn == 5 && n == guess:
			msg = "🏆 NICE - You did it again!"
		default:
			msg = "☠️  YOU LOST... Try again?"
		}
		fmt.Println(msg)
	}
}
