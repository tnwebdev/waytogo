package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	e := os.Getenv("PATH")
	fmt.Println(e)
	el := filepath.SplitList(e)

	query := os.Args[1:]

	for _, q := range query {
		for i, w := range el {
			q, w = strings.ToLower(q), strings.ToLower(w)
			if strings.Contains(q, w) {
				fmt.Printf("#%-2d: %q \n", i+1, w)
			}
		}
	}
}
