package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Incdecs
//
//  1. Increase the `counter` 5 times
//  2. Decrease the `factor` 2 times
//  3. Print the product of counter and factor
//
// RESTRICTION
//  Use only the incdec statements
//
// EXPECTED OUTPUT
//  -75
// ---------------------------------------------------------
func incdecs() {

	c, f := 45, 0.5
	c++
	c++
	c++
	c++
	c++
	fmt.Println(c)

	f--
	f--
	fmt.Println(f)

	fmt.Println(float64(c) * f)
}
