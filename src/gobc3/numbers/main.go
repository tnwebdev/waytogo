/*
	Numbers Exercies
*/

package main

import (
	"gobc3/package/sep"
)

func main() {

	// EXERCISE: Do Some Calculations
	sep.Cust("Do some calculation")
	calculate()

	// EXERCISE: Fix the Float
	sep.Cust("Fix the float")
	fixTheFloat()

	sep.Cust("Precedence")
	precedence()

	sep.Cust("Incdecs")
	incdecs()

	sep.Cust("Manipulate a Counter")
	manipCounter()

	sep.Cust("Simplify the Assignment")
	simpAssignments()

	sep.Cust("Circle Area")
	circleArea()
}
