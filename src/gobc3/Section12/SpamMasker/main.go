// Project: Build a Spam Masker
// Tasks
/*
#1 - Get and check the input
#2 - Create a byte buffer and use it as the output
#3 - Write input to the buffer as it is and print it
#4 - Detect the link
#5 - Mask the link
#6 - Stop masking when whitespace is detected
#7 - Put a http:// prefix in the front of the masked link
*/

package main

import (
	"fmt"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		fmt.Println("give me something to mask!")
		return
	}

	const (
		link  = "http://"
		nlink = len(link)
		mask  = '*'
	)

	var (
		text = args[0]
		size = len(text)
		// buf  = make([]byte, 0, size)
		buf = []byte(text)

		inLink bool
	)
	// fmt.Printf("%T %d %q\n", text, len(text), text)
	// fmt.Printf("%T %s\n", buf, (string(buf)))
	// return

	for i := 0; i < size; i++ {

		if len(text[i:]) >= nlink && text[i:i+nlink] == link {
			inLink = true
			// buf = append(buf, link...)
			i += nlink
		}

		c := text[i]

		switch c {
		case ' ', '\t', '\n':
			inLink = false
		}

		if inLink {
			buf[i] = mask
		}

		// buf = append(buf, c)
	}
	fmt.Println(string(buf))
}
