// Exercise: use slice of bytes to create string

package main

import (
	"fmt"
	"strings"
)

func main() {

	const sample = "\xbd\xb2\x3d\xbc\x20\xe2\x8c\x98"
	const sample1 = "Chương Trình Thanh Tiến Hôn Nhân Gia Đình, Giáo Phân San Jose"
	str := strings.Split(sample, "\\x")

	str1 := strings.Fields(sample1)

	fmt.Printf("%T % x\n", str, str)
	fmt.Printf("%q\n", str1)

}
