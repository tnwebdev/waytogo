// Convert index, slice bytes, rune and strings

package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {

	str := "Yũgen ☯ 💀"

	bytes := []byte(str)

	str = string(bytes)

	fmt.Printf("%s\n", str)
	fmt.Printf("\t%d bytes count\n", len(str))
	fmt.Printf("\t%d runes count\n", utf8.RuneCountInString(str))
	fmt.Printf("% x\n", bytes)
	fmt.Printf("\t%d bytes count\n", len(bytes))
	fmt.Printf("\t%d runes count\n", utf8.RuneCount(bytes))

	fmt.Println("Printing slice byte by byte")
	for i, r := range str {
		fmt.Printf("str[%2d] = % -12x = %q\n", i, string(r), r)
	}

	fmt.Println()
	fmt.Printf("1st byte     : %c\n", str[0])
	fmt.Printf("2nd byte     : %c\n", str[1])
	fmt.Printf("2nd rune     : %s\n", str[1:3])
	fmt.Printf("7th rune     : %s\n", str[7:11])
	fmt.Printf("Last rune    : %s\n", str[11:])

}
