/*
	Variable declaration example
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	var myAge, yourAge int
	var temperature float64
	var success bool
	var language string

	fmt.Println(myAge, yourAge)
	fmt.Println(temperature)
	fmt.Println(success)
	fmt.Println(language)

	sep.Cust("Declare Int")
	declareInt()

	sep.Cust("Declare Bool")
	declareBool()

	sep.Cust("Declare Float 64")
	declareFloat64()

	sep.Cust("Declare String")
	declareString()

	sep.Cust("Declare Bits")
	declareBits()

	sep.Cust("Multiple Variable Declaration 1")
	varMultiDeclare1()

	sep.Cust("Multiple Variable Declaration 2")
	varMultiDeclare2()

	sep.Cust("Un-used Variable")
	unusedVar()

	sep.Cust("Package level declaration")
	pkgScope()

	sep.Cust("Wrong order var declaration")
	wrongOrder()

}

// ---------------------------------------------------------
// EXERCISE: Declare int
//
//  1. Declare and print a variable with an int type
//
//  2. The declared variable's name should be: height
//
// EXPECTED OUTPUT
//  0
// ---------------------------------------------------------

func declareInt() {
	var height int

	fmt.Println(height)

}

// ---------------------------------------------------------
// EXERCISE: Declare bool
//
//  1. Declare and print a bool variable
//
//  2. The variable's name should be: isOn
//
// EXPECTED OUTPUT
//  false
// ---------------------------------------------------------

func declareBool() {
	var isOn bool
	fmt.Println(isOn)
}

// ---------------------------------------------------------
// EXERCISE: Declare float64
//
//  1. Declare and print a variable with a float64 type
//
//  2. The declared variable's name should be: brightness
//
// EXPECTED OUTPUT
//  0
// ---------------------------------------------------------
func declareFloat64() {
	var brightness float64

	fmt.Println(brightness)
}

// ---------------------------------------------------------
// EXERCISE: Declare string
//
//  1. Declare a string variable
//
//  2. Print that variable
//
// EXPECTED OUTPUT
//  ""
// ---------------------------------------------------------
func declareString() {
	var str string

	fmt.Printf("%s\t %T\n", str, str)
}

// ---------------------------------------------------------
// EXERCISE: Undeclarables
//
//  1. Declare the variables below:
//      3speed
//      !speed
//      spe?ed
//      var
//      func
//      package
//
//  2. Observe the error messages
//
// NOTE
//  The types of the variables are not important.
// ---------------------------------------------------------
/*
func undeclarables() {
	var 3speed float64	// Name start with number is syntax error
	var !speed int		// Name start with ! is syntax error
	var spe?ed string	// Name contain ? is syntax error

	var var string 	// Variable name is Go's keyword, not valid
	var func bool	// Variable name is a keyword, not valid
	var package string	// Variable name is a keyword

	fmt.Println(3speed) // not a valid variable name
}
*/

// ---------------------------------------------------------
// EXERCISE: Declare with bits
//
//  1. Declare a few variables using the following types
//    int
//    int8
//    int16
//    int32
//    int64
//    float32
//    float64
//    complex64
//    complex128
//    bool
//    string
//    rune
//    byte
//
// 2. Observe their output
//
// 3. After you've done, check out the solution
//    and read the comments there
//
// EXPECTED OUTPUT
//  0 0 0 0 0 0 0 (0+0i) (0+0i) false 0 0
//  ""
// ---------------------------------------------------------
func declareBits() {

	var height int
	var length int8
	var in16 int16
	var in64 int64
	var flt64 float64
	var flt32 float32
	var complx64 complex64
	var complx128 complex128
	var test bool
	var str string
	var run rune
	var bite byte

	fmt.Println(height, length, in16, in64, flt64, flt32, complx64, complx128, test, str, run, bite)
	fmt.Printf("%q\n", str)
	fmt.Println(str)
}

// ---------------------------------------------------------
// EXERCISE: Multiple
//
//  1. Declare two variables using
//     multiple variable declaration statement
//
//  2. The first variable's name should be active
//  3. The second variable's name should be delta
//
//  4. Print them all
//
// HINT
//  You should declare a bool and an int variable
//
// EXPECTED OUTPUT
//  false 0
// ---------------------------------------------------------
func varMultiDeclare1() {

	var (
		active bool
		delta  int
	)

	fmt.Println(active, delta)
}

// ---------------------------------------------------------
// EXERCISE: Multiple #2
//
//  1. Declare and initialize two string variables
//     using multiple variable declaration
//
//  2. Use the type once while declaring the variables
//
//  3. The first variable's name should be firstName
//  4. The second variable's name should be lastName
//
//  5. Print them all
//
// EXPECTED OUTPUT
//  "" ""
// ---------------------------------------------------------
func varMultiDeclare2() {

	var firstName, lastName string
	firstName = ""
	lastName = ""

	fmt.Println(firstName, lastName)
	fmt.Printf("%q %q\n", firstName, lastName)

}

// ---------------------------------------------------------
// EXERCISE: Unused
//
//  1. Declare a variable
//
//  2. Variable's name should be: isLiquid
//
//  3. Discard it using a blank-identifier
//
// NOTE
//  Do not print the variable
// ---------------------------------------------------------
func unusedVar() {

	var isLiquid bool

	_ = isLiquid
}

// ---------------------------------------------------------
// EXERCISE: Package Variable
//
//  1. Declare a variable in the package-scope
//
//  2. Observe whether something happens when you don't
//     use it
// ---------------------------------------------------------
var pkgVar = "This is a package level declaration that not being use anyway in the code"

func pkgScope() {

	// fmt.Println(pkgVar)
}

// ---------------------------------------------------------
// EXERCISE: Wrong order
//
//  1. Print a variable
//
//  2. Then declare it
//  (This means: Try to print it before its declaration)
//
//  3. Observe the error
// ---------------------------------------------------------
func wrongOrder() {

	worder := "Wrong Order Declaration!" // Correct order declaration

	fmt.Println(worder)

	// worder := "Wrong Order Declaration!" 	// Wrong order declaration
}
