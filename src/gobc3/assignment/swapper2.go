// ---------------------------------------------------------
// EXERCISE: Swapper #2
//
//  1. Swap the values of `red` and `blue` variables
//
//  2. Print them
//
// EXPECTED OUTPUT
//  blue red
// ---------------------------------------------------------

package main

import "fmt"

func swapper2() {

	red, blue := "Red", "Blue"
	fmt.Printf("Before the swap Red = %s, Blue = %s\n", red, blue)
	red, blue = blue, red

	fmt.Printf("After the swap Red = %s, Blue = %s\n", red, blue)
}
