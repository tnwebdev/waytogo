package main

import "fmt"

// ---------------------------------------------------------
// EXERCISE: Make It Blue
//
//  1. Change `color` variable's value to "blue"
//
//  2. Print it
//
// EXPECTED OUTPUT
//  blue
// ---------------------------------------------------------
func mkBlue() {
	color := "Red"

	color = "Blue"

	fmt.Println(color)

}
