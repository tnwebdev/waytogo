// ---------------------------------------------------------
// EXERCISE: Set an Alarm
//
//  Goal is printing " ALARM! " every 10 seconds.
//
// EXPECTED OUTPUT
//
//     ██   ███       ███  ██        ███  ███
//      █     █   ░     █   █    ░     █  █ █
//      █   ███       ███   █        ███  ███
//      █   █     ░     █   █    ░     █    █
//     ███  ███       ███  ███       ███  ███
//
//          ███  █    ███  ██   █ █   █
//          █ █  █    █ █  █ █  ███   █
//          ███  █    ███  ██   █ █   █
//          █ █  █    █ █  █ █  █ █
//          █ █  ███  █ █  █ █  █ █   █
//
//     ██   ███       ███  ██        █ █  ██
//      █     █   ░     █   █    ░   █ █   █
//      █   ███       ███   █        ███   █
//      █   █     ░     █   █    ░     █   █
//     ███  ███       ███  ███         █  ███
//
// HINTS
//
//  <<< BEWARE THE SPOILER! >>>
//
//  I recommend you to first solve the exercise yourself before reading the
//  following hint.
//
//
//  + You can create a new array named alarm with the same length of the
//    clocks array, so you can copy the alarm array to the clocks array
//    every 10 seconds.
//
// ---------------------------------------------------------

package main

import (
	"fmt"
	"time"

	"github.com/inancgumus/screen"
)

func main() {

	// Print the digits side by side
	for i := range digits[0] {
		for j := range digits {
			fmt.Print(digits[j][i], "  ")
		}
		fmt.Println()
	}

	// Clear the screen to refresh the clock
	screen.Clear()

	for {
		// Move the cusor to the top left corner of the screen
		screen.MoveTopLeft()

		// Get current time
		now := time.Now()

		// Get current hour, minutes, and second
		h, m, s := now.Hour(), now.Minute(), now.Second()

		// Creat a clock array by getting the digits from digits array
		clock := []placeholder{
			digits[h/10], digits[h%10],
			sep,
			digits[m/10], digits[m%10],
			sep,
			digits[s/10], digits[s%10],
		}

		// Print the clock array
		for i := range clock[0] {
			for j, d := range clock {
				next := (clock[j][i])

				if d == sep && s%2 == 0 {
					next = "   "
				}
				fmt.Print(next, "  ")
			}
			fmt.Println()
		}
		fmt.Println()

		time.Sleep(time.Second)

		// Print alarm
		for i := range alarm[0] {
			for j := range alarm {
				next := (alarm[j][i])
				if s%10 == 0 {
					fmt.Print(next, "  ")
				} else {
					screen.Clear()
				}
			}
			fmt.Println()
		}
	}
}
