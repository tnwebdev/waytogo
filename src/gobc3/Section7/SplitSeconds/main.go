// ---------------------------------------------------------
// EXERCISE: Add Split Seconds
//
//  Your goal is adding the split second to the clock. A split second is
//  1/10th of a second.
//
//  1. Find the current split second
//  2. Add dot character to the clock (as in the expected output)
//  3. Add the split second digit to the clock
//  4. Blink the dot every two seconds (just like the separators)
//  5. Update the clock every 1/10th of a second, instead of every second.
//     (Update the clock every 100 millliseconds)
//
// HINTS
//   + You can find the split second using Nanosecond method of the Time type.
//     https://golang.org/pkg/time/#Time.Nanosecond
//
//   + A split second is the first digit of the Nanosecond.
//
//   + Remember: time.Second is an integer constant, so it can be divided
//               with a number.
//
//     https://golang.org/pkg/time/#Time.Second1
//
// EXPECTED OUTPUT
//     Note that, clock is updated every split second instead of a second.
//
//     Separators are displayed (second is an odd number):
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █       █ █
//      █    █        ███   █         █     █       █ █
//      █    █    ░     █   █    ░    █     █       █ █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ██
//      █    █    ░   █     █    ░    █     █        █
//      █    █        ███   █         █     █        █
//      █    █    ░     █   █    ░    █     █        █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █         █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █       █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █         █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █         █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       █ █
//      █    █    ░   █     █    ░    █     █       █ █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █         █
//     ███  ███       ███  ███       ███    █   ░     █
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █       █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █         █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █       █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █       █ █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █         █
//      █    █        ███   █         █     █         █
//      █    █    ░     █   █    ░    █     █         █
//     ███  ███       ███  ███       ███    █   ░     █
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █       █ █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █       █ █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █    ░   █     █    ░    █     █       █ █
//      █    █        ███   █         █     █       ███
//      █    █    ░     █   █    ░    █     █         █
//     ███  ███       ███  ███       ███    █   ░   ███
//
//     Separators are not displayed (second is an even number):
//
//     ██   ██        ███  ██        ██   ███       ███
//      █    █        █     █         █   █ █       █ █
//      █    █        ███   █         █   ███       █ █
//      █    █          █   █         █   █ █       █ █
//     ███  ███       ███  ███       ███  ███       ███
//
// ---------------------------------------------------------

package main

import (
	"fmt"
	"time"

	"github.com/inancgumus/screen"
)

func main() {

	// Print the digits side by side
	for i := range digits[0] {
		for j := range digits {
			fmt.Print(digits[j][i], "  ")
		}
		fmt.Println()
	}

	// Clear the screen to refresh the clock
	screen.Clear()

	for {
		// Move the cusor to the top left corner of the screen
		screen.MoveTopLeft()

		// Get current time
		now := time.Now()

		// Get current hour, minutes, and second
		h, m, s := now.Hour(), now.Minute(), now.Second()

		// Split second
		ss := now.Nanosecond() / 1e8

		// Creat a clock array by getting the digits from digits array
		clock := []placeholder{
			digits[h/10], digits[h%10],
			sep,
			digits[m/10], digits[m%10],
			sep,
			digits[s/10], digits[s%10],
			ssep,
			digits[ss],
		}

		// Set condition for alarm
		// alarmOff := s%10 == 0

		// Print the clock array
		for i := range clock[0] {

			for j, d := range clock {
				next := (clock[j][i])

				if (d == sep || d == ssep) && s%2 == 0 {
					next = "   "
				}
				fmt.Print(next, "  ")
			}
			fmt.Println()
		}
		fmt.Println()

		// 1/10 of a second
		const splitSecond = time.Second / 10

		// sleep for 1/10 of a second
		time.Sleep(splitSecond)
	}
}
