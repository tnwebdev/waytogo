package main

// Create the place holder type to hole digit 0 - 9
type placeholder [5]string

var zero = placeholder{
	"███",
	"█ █",
	"█ █",
	"█ █",
	"███",
}

var one = placeholder{
	"██ ",
	" █ ",
	" █ ",
	" █ ",
	"███",
}

var two = placeholder{
	"███",
	"  █",
	"███",
	"█  ",
	"███",
}

var three = placeholder{
	"███",
	"  █",
	" ██",
	"  █",
	"███",
}

var four = placeholder{
	"█ █",
	"█ █",
	"███",
	"  █",
	"  █",
}

var five = placeholder{
	"███",
	"█  ",
	"███",
	"  █",
	"███",
}

var six = placeholder{
	"███",
	"█  ",
	"███",
	"█ █",
	"███",
}

var seven = placeholder{
	"███",
	"  █",
	"  █",
	"  █",
	"  █",
}

var eight = placeholder{
	"███",
	"█ █",
	"███",
	"█ █",
	"███",
}

var nine = placeholder{
	"███",
	"█ █",
	"███",
	"  █",
	"███",
}

var sep = placeholder{
	"   ",
	" ░ ",
	"   ",
	" ░ ",
	"   ",
}

var ssep = placeholder{
	"   ",
	"   ",
	"   ",
	"   ",
	" ░ ",
}

// Create place holder for alphabets A-Z

var a = placeholder{
	"███",
	"█ █",
	"███",
	"█ █",
	"█ █",
}

var l = placeholder{
	"█  ",
	"█  ",
	"█  ",
	"█  ",
	"███",
}

var r = placeholder{
	"██ ",
	"█ █",
	"██ ",
	"█ █",
	"█ █",
}

var m = placeholder{
	"█ █",
	"███",
	"█ █",
	"█ █",
	"█ █",
}

var ex = placeholder{
	"█  ",
	"█  ",
	"█  ",
	"   ",
	"█  ",
}

var space = placeholder{
	"   ",
	"   ",
	"   ",
	"   ",
	"   ",
}

// Store the digits in digits slice
var digits = []placeholder{
	zero, one, two, three, four, five, six, seven, eight, nine,
}

// Store the alarm letters
var alarm = []placeholder{
	space, a, r, l, a, r, m, ex, space,
}
