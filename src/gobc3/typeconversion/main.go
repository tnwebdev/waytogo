/*
	Type Conversion Exercises
*/

package main

import (
	"gobc3/package/sep"
)

func main() {

	sep.Cust("Convert And Fix 1")
	convertAndFix()

	sep.Cust("Convert And Fix 2")
	convertAndFix2()

	sep.Cust("Convert And Fix 3")
	convertAndFix3()

	sep.Cust("Convert And Fix 4")
	convertAndFix4()

	sep.Cust("Convert And Fix 5")
	convertAndFix5()

}
