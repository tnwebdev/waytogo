package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Leap Year
//
//  Find out whether a given year is a leap year or not.
//
// EXPECTED OUTPUT
//  go run main.go
//    Give me a year number
//
//  go run main.go eighties
//    "eighties" is not a valid year.
//
//  go run main.go 2018
//    2018 is not a leap year.
//
//  go run main.go 2100
//    2100 is not a leap year.
//
//  go run main.go 2019
//    2019 is not a leap year.
//
//  go run main.go 2020
//    2020 is a leap year.
//
//  go run main.go 2024
//    2024 is a leap year.
// ---------------------------------------------------------

const (
	noarg = `
This program help find the leap year, please enter the command and the year number then press enter.

usage: command [year] 	Enter the year number in positive integer.
Example: command 2016
`
	errnum = "is not a number"
	leap   = "is a leap year"
	noLeap = "is not a leap year"
)

func main() {

	if arg := os.Args; len(arg) != 2 {
		fmt.Println(noarg)
	} else if n, err := strconv.Atoi(arg[1]); err != nil {
		fmt.Println(arg[1], errnum, n)
	} else if n%4 == 0 && !(n%400 == 0) {
		fmt.Println(n, leap)
	} else {
		fmt.Println(n, noLeap)
	}

}
