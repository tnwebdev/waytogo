package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Only Evens
//
//  1. Extend the "Sum up to N" exercise
//  2. Sum only the even numbers
//
// RESTRICTIONS
//  Skip odd numbers using the `continue` statement
//
// EXPECTED OUTPUT
//  Let's suppose that the user runs it like this:
//
//    go run main.go 1 10
//
//  Then it should print:
//
//    2 + 4 + 6 + 8 + 10 = 30
// ---------------------------------------------------------

func main() {

	var sum int

	if len(os.Args) != 3 {
		fmt.Println("Give me a minimum and a maximum numbers!")
		fmt.Println("Example: Command [1] [10]")
		fmt.Println("This command only sums up the even numbers")
		return
	}

	min, err1 := strconv.Atoi(os.Args[1])
	max, err2 := strconv.Atoi(os.Args[2])
	if err1 != nil || err2 != nil || min >= max {
		fmt.Println("Wrong numbes")
		return
	}

	fmt.Println(min, max)

	for i := min; i <= max; i++ {
		if i%2 == 0 {
			fmt.Print(i)
			if i < max {
				fmt.Print(" + ")
			}
			sum += i
		}
	}
	fmt.Printf(" = %d\n", sum)
}
