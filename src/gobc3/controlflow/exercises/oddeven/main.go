package main

import (
	"fmt"
	"os"
	"strconv"
)

// ---------------------------------------------------------
// EXERCISE: Odd or Even
//
//  1. Get a number from the command-line.
//
//  2. Find whether the number is odd, even and divisible by 8.
//
// RESTRICTION
//  Handle the error. If the number is not a valid number,
//  or it's not provided, let the user know.
//
// EXPECTED OUTPUT
//  go run main.go 16
//    16 is an even number and it's divisible by 8
//
//  go run main.go 4
//    4 is an even number
//
//  go run main.go 3
//    3 is an odd number
//
//  go run main.go
//    Pick a number
//
//  go run main.go ABC
//    "ABC" is not a number
// ---------------------------------------------------------
const (
	noarg = `
usage: command [number] 	Enter an in positive integer.
Example: command 18
`
	even       = "is an even number"
	divisible8 = "divisible by 8"
	odd        = "is an odd number"
	errnum     = "is not a number"
)

func main() {

	// Get a number from the command-line
	// Handle input error. If the number is not valid or no argument provided, let the user know.
	if arg := os.Args; len(arg) != 2 {
		fmt.Println(noarg) // output usage: instruction
	} else if n, err := strconv.Atoi(arg[1]); err != nil {
		fmt.Println(arg[1], errnum, n) // Arg is not a number
	} else if n%2 == 0 && n%8 == 0 {
		fmt.Println(n, even, "and", divisible8) // arg even and divisible by 8
	} else if n%2 == 0 {
		fmt.Println(n, even) // arg is an even number
	} else {
		fmt.Println(n, odd) // arg is odd number
	}
}
