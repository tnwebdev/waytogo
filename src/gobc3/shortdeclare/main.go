/*
	Short Declaration Exercies
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
)

func main() {

	sep.Cust("Short Declare") // Output separator
	shortDeclare()

	sep.Cust("Multi-Short Declare") // Output separator
	multiShortDeclare()

	sep.Cust("Multi-Short Declare 2") // Output separator
	multiShortDeclare1()

	sep.Cust("Short Declare With Expression") // Output separator
	shortDeclareExpression()

	sep.Cust("Short Declare - Discard") // Output separator
	shortDiscard()

	sep.Cust("Redeclare") // Output separator
	redeclare()

}

// ---------------------------------------------------------
// EXERCISE: Short Declare
//
//  Declare and then print four variables using
//  the short declaration statement.
//
// EXPECTED OUTPUT
//  i: 314 f: 3.14 s: Hello b: true
// ---------------------------------------------------------
func shortDeclare() {

	i := 315
	f := 3.14
	s := "Hello"
	b := true

	fmt.Printf("i: %v f: %v s: %v b: %v\n", i, f, s, b)
}

// ---------------------------------------------------------
// EXERCISE: Multiple Short Declare
//
//  Declare two variables using multiple short declaration
//
// EXPECTED OUTPUT
//  14 true
// ---------------------------------------------------------
func multiShortDeclare() {

	i, b := 14, true

	fmt.Println(i, b)
}

// ---------------------------------------------------------
// EXERCISE: Multiple Short Declare #2
//
//  1. Declare two variables using multiple short declaration
//
//  2. `a` variable's value should be 42
//  3. `c` variable's value should be "good"
//
// EXPECTED OUTPUT
//  42 good
// ---------------------------------------------------------
func multiShortDeclare1() {

	a, c := 42, "good"

	fmt.Println(a, c)
}

// ---------------------------------------------------------
// EXERCISE: Short With Expression
//
// 	1. Short declare a variable named `sum`
//
//  2. Initialize it with an expression by adding 27 and 3.5
//
// EXPECTED OUTPUT
//  30.5
// ---------------------------------------------------------
func shortDeclareExpression() {

	sum := 27 + 3.5

	fmt.Println(sum)
}

// ---------------------------------------------------------
// EXERCISE: Short Discard
//
// 	1. Short declare two bool variables
//     (use multiple short declaration syntax)
//
//  2. Initialize both variables to true
//
//  3. Change your declaration and
//     discard the 2nd variable's value
//     using the blank-identifier
//
//  4. Print only the 1st variable
//
// EXPECTED OUTPUT
//  true
// ---------------------------------------------------------
func shortDiscard() {

	b1, b2 := true, true

	_ = b2

	fmt.Println(b1)
}

// ---------------------------------------------------------
// EXERCISE: Redeclare
//
// 	1. Short declare two int variables: age and yourAge
//     (use multiple short declaration syntax)
//
//  2. Short declare a new float variable: ratio
//     And, change the 'age' variable to 42
//
//     (! You should use redeclaration)
//
//  4. Print all the variables
//
// EXPECTED OUTPUT
//  42, 20, 3.14
// ---------------------------------------------------------
func redeclare() {

	age, yourAge := 35, 20

	ratio, age := 3.14, 42

	fmt.Println(age, yourAge, ratio)

}
