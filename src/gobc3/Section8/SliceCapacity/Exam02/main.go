// Slice capacity example code

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintBacking = true

	// create a nil slice and print out len, cap, and pointer
	var games []string
	s.Show("Games", games)

	// Assign an empty slice to games
	games = []string{}
	s.Show("Game", games)

	// Creat an empty slice on the fly
	s.Show("another empty", []int{})

	// Assign some elements to empty slice
	games = []string{"pacman", "mario", "tetris", "doom"}
	s.Show("games", games)

	// Assign games slice to part variable, now part and games
	// are both point to the same backing arrage.
	part := games
	s.Show("part", part)

	// Modify part and game slice
	part = games[:0]
	s.Show("part[:0]", part)
	games = games[0:2]
	s.Show("games", games)

	// restore the slice to full capacity
	part = games[0:4]
	s.Show("part[0:]", part)
	games = games[0:4]
	s.Show("games", games)

	for cap(part) != 0 {
		part = part[1:cap(part)]
		s.Show("part", part)
	}

	/*
		// Now assign and empty value to games
		games = []string{}
		fmt.Printf("Len: %v, cap: %v, pointer: %p\n", len(games), cap(games), &games)

		// Now assign some actual value to []games
		games = []string{"Packman", "Mario", "Tetris", "Doom"}
		fmt.Printf("games: %#v\n", games)
		fmt.Printf("Len: %v, cap: %v, pointer: %p\n", len(games), cap(games), &games)

		// Now let slice the games clice with [:0]
		part := games
		fmt.Printf("part %#v pointer: %p\n", part, &part)

		part = games[:0]

		// part is now showing [] slice
		fmt.Printf("part[:0] %v pointer: %p\n", part, &part)

		// part still showing capacity of 4 element
		fmt.Printf("part[:cap %v\n", part[:cap(part)])

		// games slise is still showing original backing array
		fmt.Printf("games %q pointer: %p\n", games, &games)

		// now let loop through part slice and print capacity and it's values
		for cap(part) != 0 {
			part = part[1:cap(part)]
			fmt.Printf("part %v\n", part)
			fmt.Printf("Part pointer %p\n", &part)
		}

		fmt.Println(unsafe.Sizeof(""))
	*/

}
