package main

import (
	"fmt"
	"strconv"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Housing Prices and Averages
//
//  Use the previous exercise to solve this exercise (Housing Prices).
//
//  Your task is to print the averages of the sizes, beds, baths, and prices.
//
//
// EXPECTED OUTPUT
//
//  Location       Size           Beds           Baths          Price
//  ===========================================================================
//  New York       100            2              1              100000
//  New York       150            3              2              200000
//  Paris          200            4              3              400000
//  Istanbul       500            10             5              1000000
//
//  ===========================================================================
//                 237.50         4.75           2.75           425000.00
//
// ---------------------------------------------------------

func main() {
	const (
		header = "Location,Size,Beds,Baths,Price"
		data   = `New York,100,2,1,100000
New York,150,3,2,200000
Paris,200,4,3,400000
Istanbul,500,10,5,1000000`

		sep = ","
	)

	var (
		locs                       []string
		sizes, beds, baths, prices []int
	)

	// Create the header label slice
	headLabel := strings.Split(header, sep)

	// Create the data slice
	rows := strings.Split(data, "\n")

	for _, row := range rows {
		cols := strings.Split(row, sep)

		locs = append(locs, cols[0])

		n, _ := strconv.Atoi(cols[1])
		sizes = append(sizes, n)

		n, _ = strconv.Atoi(cols[2])
		beds = append(beds, n)

		n, _ = strconv.Atoi(cols[3])
		baths = append(baths, n)

		n, _ = strconv.Atoi(cols[4])
		prices = append(prices, n)
	}

	// Print the header labels
	fmt.Println()
	for _, h := range headLabel {
		fmt.Printf("%-15s", h)
	}

	// Print the separator lines
	fmt.Printf("\n%s\n", strings.Repeat("=", 75))

	// Print colomns data
	for i := range rows {
		fmt.Printf("%-15s", locs[i])
		fmt.Printf("%-15d", sizes[i])
		fmt.Printf("%-15d", beds[i])
		fmt.Printf("%-15d", baths[i])
		fmt.Printf("%-15d", prices[i])
		fmt.Println()
	}

	// Calucalte averages for sizes, beds, baths, and prices
	var tsize, tbed, tbath, tprice float64

	for i := range rows {
		tsize += float64(sizes[i])
		tbed += float64(beds[i])
		tbath += float64(baths[i])
		tprice += float64(prices[i])

	}

	// Print the separator
	fmt.Printf("%s\n", strings.Repeat("=", 75))

	asize := tsize / float64(len(rows))
	abed := tbed / float64(len(rows))
	abath := tbath / float64(len(rows))
	aprice := tprice / float64(len(rows))
	fmt.Printf("%-14v %-14.2f %-14.2f %-14.2f %-14.2f\n", "", asize, abed, abath, aprice)
	fmt.Println()
}
