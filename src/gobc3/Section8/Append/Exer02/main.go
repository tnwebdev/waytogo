// Append Exercies
// Exercices Level 2 - Appending

package main

import (
	"fmt"
	"time"
)

// ---------------------------------------------------------
// EXERCISE: Append #2
//
//  1. Create the following nil slices:
//     + Pizza toppings
//     + Departure times
//     + Student graduation years
//     + On/off states of lights in a room
//
//  2. Append them some elements (use your creativity!)
//
//  3. Print all the slices
//
//
// EXPECTED OUTPUT
// (Your output may change, mine is like so:)
//
//  pizza       : [pepperoni onions extra cheese]
//
//  graduations : [1998 2005 2018]
//
//  departures  : [2019-01-28 15:09:31.294594 +0300 +03 m=+0.000325020
//  2019-01-29 15:09:31.294594 +0300 +03 m=+86400.000325020
//  2019-01-30 15:09:31.294594 +0300 +03 m=+172800.000325020]
//
//  lights      : [true false true]
//
//
// HINTS
//  + For departure times, use the time.Time type. Check its documentation.
//
//      now := time.Now()     -> Gives you the current time
//      now.Add(time.Hour*24) -> Gives you a time.Time 24 hours after `now`
//
//  + For graduation years, you can use the int type
// ---------------------------------------------------------

func main() {

	now := time.Now()

	pizza := []string{"Pepperoni", "onions", "extra cheese"}
	graduations := []int{1998, 2005, 2018}
	departures := []time.Time{now, now.Add(time.Hour * 24), now.Add(time.Hour * 48)}
	lights := []bool{true, false, true}

	fmt.Printf("Pizza topping: %v\n", pizza)
	fmt.Printf("\nGraduation years: %v\n", graduations)
	fmt.Printf("\nDeparture time: %v\n", departures)
	fmt.Printf("\nLights state: %v\n", lights)

}
