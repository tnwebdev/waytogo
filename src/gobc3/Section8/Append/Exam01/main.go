// Append function
// append(slice, newElement)

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintElementAddr = true
	s.MaxPerLine = 8

	// Creat a nums slice
	nums := []int{1, 2}

	// Add a new element to nums slice using append
	// Note that go increat space by binary 2, 4, 8, 16
	nums = append(nums, 4)
	s.Show("nums:", nums)

	// Add two more elements
	nums = append(nums, 5, 6)
	s.Show("nums:", nums)

	// Now let append two element in the middle
	nums = append(nums, nums[2:]...)
	s.Show("nums[2:]", nums)

	// Add the two lement 99, 99
	nums = append(nums[:2], 99, 99, 99)
	s.Show("nums", nums)

	nums = nums[0:8]
	s.Show("nums", nums)

}
