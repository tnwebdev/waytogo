// When does the append function create a new backing array?

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintElementAddr = true
	s.MaxPerLine = 8

	// Let create a nil slice and print it out
	var nums []int
	s.Show("nums", nums)

	// Now let append some elments to nums slice
	nums = append(nums, 1, 2)
	s.Show("nums", nums)

	// Let try to add one more elements
	nums = append(nums, 3)
	s.Show("nums", nums)

	nums = append(nums, 4, 5)
	s.Show("nums", nums)

	// Let do a append trick by copy the elements from index #2 to end of the slice to nums
	nums = append(nums, nums[2:]...)
	s.Show("nums[2:]...", nums)

	// When append new alements, append function always put the new elements to the end of the slice
	// Now let try to append some elements in the middle of the slice.
	nums = append(nums[:2], 0, 0, 0)
	nums = nums[:8]
	s.Show("nums[:2], 0, 0, 0", nums)

	// Now let add some more elements to see if append adding 8 more capacity to nums
	nums = nums[0:8]
	s.Show("nums[0:8]", nums)
}
