// Random number generator

package main

import (
	"fmt"
	"math/rand"
	"os"
	"sort"
	"strconv"
	"time"
)

func main() {
	// Random seed number ganarator
	rand.Seed(time.Now().UnixNano())

	args := os.Args[1:]
	if len(args) < 1 {
		fmt.Println("Usage: command [5]")
		return
	}
	// Reading the input from command-line
	num, err := strconv.Atoi(args[0])
	if err != nil {
		fmt.Println("not number")
		return
	}

	// Validate the command-line input
	if num < 1 {
		fmt.Println("Please enter a positive number")
		return
	}

	// Slice to hold unique numbers
	var uniques []int

loop: // label
	for len(uniques) < num {
		n := rand.Intn(num) + 1
		fmt.Print(n, " ")

		// skip the repeated numbers
		for _, u := range uniques {
			if u == n {
				continue loop // jump back to loop label
			}
		}

		// store the number to uniques slice
		uniques = append(uniques, n)
	}
	fmt.Println("\n\nUniques:", uniques)

	// Let sort the unique numbers
	sort.Ints(uniques)
	fmt.Println("Sorted uniques numbers:", uniques)

	// We can also sort and array
	arr := [8]int{3, 5, 9, 23, 45, 2, 12, 344}
	fmt.Println("arr: ", arr)
	// sort.Int(arr) // This will not work be cause sort.Int expect and sort.Int([]int)
	sort.Ints(arr[:]) // will convert the array into slice
	fmt.Println("sorted arr: ", arr)
}
