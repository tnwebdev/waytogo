package main

import (
	"fmt"
	"strconv"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Housing Prices and Averages
//
//  Use the previous exercise to solve this exercise (Housing Prices).
//
//  Your task is to print the averages of the sizes, beds, baths, and prices.
//
//
// EXPECTED OUTPUT
//
//  Location       Size           Beds           Baths          Price
//  ===========================================================================
//  New York       100            2              1              100000
//  New York       150            3              2              200000
//  Paris          200            4              3              400000
//  Istanbul       500            10             5              1000000
//
//  ===========================================================================
//                 237.50         4.75           2.75           425000.00
//
// ---------------------------------------------------------

func main() {

	const (
		header = "Location,Size,Beds,Baths,Price"
		data   = `New York,100,2,1,100000
New York,150,3,2,200000
Paris,200,4,3,400000
Istanbul,500,10,5,1000000`

		separator = ","
	)

	var (
		location                   []string
		sizes, beds, baths, prices []int
	)

	headArr := strings.Split(header, separator)

	dataArr := strings.Split(data, "\n")

	for _, d := range dataArr {
		index := strings.Split(d, separator)

		// fmt.Printf("%#v\n", index)

		location = append(location, index[0])

		n, _ := strconv.Atoi(index[1])
		sizes = append(sizes, n)

		n, _ = strconv.Atoi(index[2])
		beds = append(beds, n)

		n, _ = strconv.Atoi(index[3])
		baths = append(baths, n)

		n, _ = strconv.Atoi(index[4])
		prices = append(prices, n)
	}

	fmt.Println()

	for _, h := range headArr {
		fmt.Printf("%-15s", h)
	}
	fmt.Printf("\n%s\n", strings.Repeat("=", 70))

	for i := range dataArr {
		fmt.Printf("%-15s", location[i])
		fmt.Printf("%-15d", sizes[i])
		fmt.Printf("%-15d", beds[i])
		fmt.Printf("%-15d", baths[i])
		fmt.Printf("%-15d", prices[i])
		fmt.Println()
	}

	var sizesSum, bedsSum, bathsSum, pricesSum float64
	n := float64(len(sizes))
	fmt.Printf("%s\n", strings.Repeat("=", 70))
	for i := range sizes {
		sizesSum += float64(sizes[i])
		bedsSum += float64(beds[i])
		bathsSum += float64(baths[i])
		pricesSum += float64(prices[i])
	}

	fmt.Printf("%-15s", "")
	fmt.Printf("%-15.2f", sizesSum/n)
	fmt.Printf("%-15.2f", bedsSum/n)
	fmt.Printf("%-15.2f", bathsSum/n)
	fmt.Printf("%-15.2f", pricesSum/n)

	fmt.Println()
	fmt.Println()

	// Solve this exercise by using your previous solution for
	// the "Housing Prices" exercise.
}
