// How to use multi-dimentional slices

package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	spendings := fetch()

	var total int
	for i, d := range spendings {
		for _, v := range d {
			total += v
		}
		fmt.Printf("Daily total: #%d  %v\n", i+1, total)
	}
}

func fetch() [][]int {

	data := `10 10
30 30 30 30
20 20 20 20 20
40 40 40
50 50 50 50
60 60`

	lines := strings.Split(data, "\n")

	spendings := make([][]int, len(lines))

	for i, l := range lines {
		fmt.Printf("Daily before string split: %d, %#v\n", i+1, l)

		fields := strings.Fields(l)

		// Initialize the spending [][]int{[]int(nil)} slice
		spendings[i] = make([]int, len(fields))

		for j, f := range fields {

			// convert string to int
			amt, _ := strconv.Atoi(f)

			// Store the amount into [i][j]spendings multi-slice
			spendings[i][j] = amt

		}

	}

	// Print out the [][]spendings multi-dimentional slices
	for _, d := range spendings {
		fmt.Printf("Daily spendings: %#v\n", d)
	}

	return spendings
}
