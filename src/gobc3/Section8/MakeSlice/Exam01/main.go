// make() : Preallocate backing array

package main

import (
	"strings"

	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintElementAddr = true
	s.MaxPerLine = 8

	tasks := []string{"jump", "run", "read", "swim", "hike"}

	// var upTasks []string
	upTasks := make([]string, 0, len(tasks))
	s.Show("upTasks", upTasks)

	for _, task := range tasks {
		upTasks = append(upTasks, strings.ToUpper(task))
		s.Show("upTasks", upTasks)
	}

	tasks = append(tasks, "tennis", "soccer")
	s.Show("upTasks", tasks)

	upTasks = append(upTasks, "volley ball", "basket ball", "base ball", "ping pong")
	s.Show("tasks", upTasks)
}
