// Slice Internals - Slice Header - Example 01

package main

import (
	"fmt"
	"unsafe"
)

type collection []string

func main() {

	data := collection{"slices", "are", "awesome", "period", "add more data"}
	change(data)
	fmt.Println("check data", data)
	fmt.Printf("main's data slice address: %p\n", &data)

	array := [...]string{"slice", "are", "awesome", "period", "add more data"}
	fmt.Printf("array's size: %d bytes\n", unsafe.Sizeof(array))
	fmt.Printf("slice's size: %d bytes\n", unsafe.Sizeof(data))
}

func change(data collection) {

	data[2] = "brilliant!"
	fmt.Println("Change's data", data)
	fmt.Printf("main's data slice address: %p\n", &data)

}
 