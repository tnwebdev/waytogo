// Full sllice Expression: Limit the capacity of slice

package main

import (
	s "github.com/inancgumus/prettyslice"
)

func main() {

	s.PrintElementAddr = true
	s.MaxPerLine = 8

	nums := []int{1, 3, 2, 4}
	// odds := nums[:2:2]

	// Refactor this line
	odds := append(nums[:2:2], 5, 7)

	// Let add to more even number to the slice
	evens := append(nums[2:4], 6, 8)

	s.Show("nums", nums)
	s.Show("odds", odds)
	s.Show("evens", evens)
}
