// Slice Expressions
// Note: slicing a slice will return a new slice with selected elements. The original slice will not change.

package main

import (
	"fmt"

	s "github.com/inancgumus/prettyslice"
)

func main() {

	items := []string{
		"packman", "mario", "tetris", "doom",
		"galaga", "frogger", "astreroids", "simcity",
		"metroid", "defender", "rayman", "tempest",
		"ultima",
	}

	s.MaxPerLine = 4
	s.Show("items", items)

	// Get the first 3 items
	top3 := items[0:3]
	s.Show("Top 3 items", top3)

	// Get the last from items from items slice
	last4 := items[9:]
	s.Show("Last 4 items", last4)

	// different way to get the last 4 items
	l := len(items)
	l4 := items[9:l]
	s.Show("Last 4 items", l4)

	// Slicing a slice is call reslicing
	mid2 := l4[1:3]
	s.Show("middle 2 from l4 slice", mid2)

	fmt.Printf("slicing : %T %[1]q\n", items[2:3])
	fmt.Printf("indexing : %T %[1]q\n", items[2])
}
