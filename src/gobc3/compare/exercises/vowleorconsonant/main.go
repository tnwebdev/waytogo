package main

import (
	"fmt"
	"os"
	"strings"
)

// ---------------------------------------------------------
// EXERCISE: Vowel or Consonant
//
//  Detect whether a letter is vowel or consonant.
//
// NOTE
//  y or w is called a semi-vowel.
//  Check out: https://en.oxforddictionaries.com/explore/is-the-letter-y-a-vowel-or-a-consonant/
//
// HINT
//  + You can find the length of an argument using the len function.
//
//  + len(os.Args[1]) will give you the length of the 1st argument.
//
// BONUS
//  Use strings.IndexAny function to detect the vowels.
//  Search on Google for: golang pkg strings IndexAny
//
// EXPECTED OUTPUT
//  go run main.go
//    Give me a letter
//
//  go run main.go hey
//    Give me a letter
//
//  go run main.go a
//    "a" is a vowel.
//
//  go run main.go y
//    "y" is sometimes a vowel, sometimes not.
//
//  go run main.go w
//    "w" is sometimes a vowel, sometimes not.
//
//  go run main.go x
//    "x" is a consonant.
// ---------------------------------------------------------
func main() {

	a := os.Args

	al := len(a) - 1
	// fmt.Println(al)

	if al < 1 {
		fmt.Println("Give me a letter")
		return
	}

	s := a[1]

	s = strings.ToLower(s)

	if s == "s" || s == "e" || s == "i" || s == "o" || s == "u" {
		fmt.Printf("%s is a vowle.\n", s)
	} else if s == "y" || s == "w" {
		fmt.Printf("%s is sometime vowle, sometime not.\n", s)
	} else {
		fmt.Printf("%s is a consonant.\n", s)
	}

}
