/*
	Argument Count Exercise
*/

package main

import (
	"fmt"
	"os"
	"strings"
)

// ------------------------------------------------------------
// EXERCISE: Arg Count
//
//  1. Get arguments from command-line.
//  2. Print the expected outputs below depending on the number
//     of arguments.
//
// 	EXPECTED OUTPUT
//  go run main.go
//    Give me args
//
//  go run main.go hello
//    There is one: "hello"
//
//  go run main.go hi there
//    There are two: "hi there"
//
//  go run main.go i wanna be a gopher
//    There are 5 arguments
// ------------------------------------------------------------
func main() {

	a := os.Args[1:]
	al := len(a)

	if al < 1 {
		fmt.Println("Give me args")
	} else if al == 1 {
		fmt.Printf("There is %v: %v\n", al, a)
	} else {
		fmt.Printf("There are %v: %v\n", al, strings.Join(os.Args[1:], " "))
	}
}
