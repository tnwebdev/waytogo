/*
	Exercise 01 - Age
*/

package main

import "fmt"

func main() {

	var input float64
	fmt.Print("Enter age: ")
	fmt.Scanf("%f", &input)

	if input > 60 {
		fmt.Println("Getting older!")
	} else if input > 30 {
		fmt.Println("Getting wiser!")
	} else if input > 20 {
		fmt.Println("Adult hood!")
	} else if input > 10 {
		fmt.Println("Young blood!")
	} else {
		fmt.Println("Booting up!")
	}
}
