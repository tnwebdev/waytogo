// Project: Animate a Bouncing Ball

package main

import "fmt"

func main() {

	// Declar constant data for width and height
	const (
		width    = 50
		height   = 10
		cellBall = '🥎'
		space    = ' '
	)

	var (
		px, py int
		cell   rune
	)

	buf := make([]rune, 0, width*height)

	// Declare a place holder for the cell veriable
	// Rune will be able to hold multiple bytes
	board := make([][]bool, width)

	for column := range board {
		board[column] = make([]bool, height)
	}

	px++
	py++
	board[px][py] = true

	// draw the board
	for y := range board[0] {
		for x := range board {
			cell = space
			if board[x][y] {
				cell = cellBall
			}
			buf = append(buf, cell, ' ')
		}
		buf = append(buf, '\n')
	}
	fmt.Print(string(buf))
}
