package main

import (
	"fmt"
	"os"
	"strings"
	"unicode/utf8"
)

/*
	strecho.go: Prints the command-line arguments follow with the number of '!' base on the length of the string in the argument
*/

func strEchoBang() {

	arg := os.Args[1]
	fmt.Println(arg)

	l := utf8.RuneCountInString(arg)

	r := strings.Repeat("!", l)

	s := r + arg + r
	s = strings.ToUpper(s)

	fmt.Println(s)

	s = strings.ToLower(s)
	fmt.Println(s)

}
