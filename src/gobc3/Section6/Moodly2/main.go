// Exercise - Moodly 2 - Multidimentional Array

package main

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main() {
	args := os.Args[1:]

	if len(args) < 2 {
		fmt.Println("Please enter your [name][mood]")
		return
	}

	name := args[0]
	mood := strings.ToLower(args[1])

	moods := [...][10]string{
		{
			"good 👍",
			"happy 😀",
			"awesome 😎",
			"love 🥰",
			"Joy 😇",
			"Hope 🥳",
			"Pride ✊",
			"Interest 🤲",
			"Amusement 😊",
			"Inspiration 👍",
		},
		{
			"bad ☹️",
			"sad 😞",
			"terrible 😩",
			"Fear 😨",
			"Anger 😡",
			"Rage 😧",
			"Loneliness 😴",
			"Melancholy 😉",
			"Disgust 🤭",
			"Annoyance 🤯",
		},
	}

	rand.Seed(time.Now().UnixNano())
	m := rand.Intn(len(moods[0]))

	/*
		// Traditional if, if else
		if mood == "negative" {
			fmt.Printf("%v feels %v\n", name, moods[1][m])
		} else if mood == "positive" {
			fmt.Printf("%v feels %v\n", name, moods[0][m])
		} else {
			fmt.Println("Please enter mood as [positive|negative]")
		}
	*/

	// Refactor the if block
	var mi int
	if mood == "positive" {
		mi = 0
	} else if mood == "negative" {
		mi = 1
	} else {
		fmt.Println("Please enter mood as [positive|negative]")
		return
	}

	fmt.Printf("%v feels %v\n", name, moods[mi][m])
}
