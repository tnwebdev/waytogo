// Unamed Type - Example 01

package main

import "fmt"

func main() {

	type (
		bookcase [5]int // Named type
		cabinet  [5]int // Named type are different event the underlaying type are the same
	)

	blue := bookcase{6, 9, 3, 2, 1}
	red := cabinet{6, 9, 3, 2}

	fmt.Print("Are they equal? ")

	if blue == bookcase(red) { // blue and red only compareable when convert to the same named type
		fmt.Println("✅")
	} else {
		fmt.Println("❌")
	}

	fmt.Printf("blue: %#v\n", blue)
	fmt.Printf("red: %#v\n", red)
}
