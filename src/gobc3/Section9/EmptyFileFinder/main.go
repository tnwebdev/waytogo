// Section 9: Project - Write a File Finder
// Build an empty file finder

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {

	args := os.Args[1:]
	// Check for valid input
	if len(args) == 0 {
		fmt.Println("Please enter a directory")
		return
	}

	// Read the directory and return two values,
	// the first value is the file information, and an error
	files, err := ioutil.ReadDir(args[0])

	// check for the error, if there is error, print the err message
	if err != nil {
		log.Fatal(err)
		fmt.Println(err)
		return
	}

	numOfFile := 0

	// Allocate memory to the program
	// Method 1
	// total := len(files) * 256

	// Method 2
	var total int

	for _, f := range files {
		if f.Size() == 0 {
			total += len(f.Name()) + 1
		}
	}
	fmt.Printf("Total required memory: %d bytes.\n", total)

	// Create a holder for all the files name
	fileName := make([]byte, 0, total)
	fileName1 := make([]byte, 0, total)

	// Range over list of file and print the file name
	for i, f := range files {
		numOfFile = i + 1

		if f.Size() == 0 {
			name := f.Name()
			fileName = append(fileName, name...)
			fileName = append(fileName, '\n')
		} else {
			name1 := f.Name()
			fileName1 = append(fileName1, name1...)
			fileName1 = append(fileName1, '\n')
		}
	}

	// Write files name to a file
	err1 := ioutil.WriteFile("output.txt", fileName, 0644)
	if err1 != nil {
		fmt.Println(err1)
		return
	}

	err2 := ioutil.WriteFile("output1.txt", fileName1, 0644)
	if err2 != nil {
		fmt.Println(err2)
		return
	}

	// Print the list of empty files
	fmt.Printf("List of empty files:\n\n%s", fileName)
	fmt.Println()
	fmt.Printf("List of non-empty files:\n\n%s", fileName1)
	fmt.Println()
	fmt.Printf("Number of items in the directory: %d\n", numOfFile)
}
