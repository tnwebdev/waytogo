/*
	PRINTF EXERCISES
*/

package main

import (
	"fmt"
	"gobc3/package/sep"
	"os"
)

func main() {

	sep.Cust("Print Age Exercise")
	printAge()

	sep.Cust("Print Name Exercise")
	printName()

	sep.Cust("Print False Claims")
	falseClaims()

	sep.Cust("Print the Temperature")
	currentTemp()

	sep.Cust("Print Double Quotes")
	doubleQuotes()

	sep.Cust("Print Type 1")
	printType1()

	sep.Cust("Print Type 2")
	printType2()

	sep.Cust("Print Type 3")
	printType3()

	sep.Cust("Print Type 4")
	printType4()

	sep.Cust("Print full name")
	printName2()
}

// ---------------------------------------------------------
// EXERCISE: Print Your Age
//
//  Print your age using Printf
//
// EXPECTED OUTPUT
//  I'm 30 years old.
//
// NOTE
//  You should change 30 to your age, of course.
// ---------------------------------------------------------
func printAge() {
	age := 35
	fmt.Printf("I'm %v years old.\n", age)
}

// ---------------------------------------------------------
// EXERCISE: Print Your Name and LastName
//
//  Print your name and lastname using Printf
//
// EXPECTED OUTPUT
//  My name is Inanc and my lastname is Gumus.
//
// BONUS
//  Store the formatting specifier (first argument of Printf)
//    in a variable.
//  Then pass it to printf
// ---------------------------------------------------------
func printName() {
	first, last := "Chris", "Nguyen"

	fmt.Printf("My name is %v %v.\n", first, last)
}

// ---------------------------------------------------------
// EXERCISE: False Claims
//
//  Use printf to print the expected output using a variable.
//
// EXPECTED OUTPUT
//  These are false claims.
// ---------------------------------------------------------
func falseClaims() {

	var c bool

	fmt.Printf("These are %t claims\n", c)
}

// ---------------------------------------------------------
// EXERCISE: Print the Temperature
//
//  Print the current temperature in your area using Printf
//
// NOTE
//  Do not use %v verb
//  Output "shouldn't" be like 29.500000
//
// EXPECTED OUTPUT
//  Temperature is 29.5 degrees.
// ---------------------------------------------------------
func currentTemp() {
	t := 70.5

	fmt.Printf("Temperature is %.2f\n", t)
}

// ---------------------------------------------------------
// EXERCISE: Double Quotes
//
//  Print "hello world" with double-quotes using Printf
//  (As you see here)
//
// NOTE
//  Output "shouldn't" be just: hello world
//
// EXPECTED OUTPUT
//  "hello world"
// ---------------------------------------------------------
func doubleQuotes() {

	fmt.Printf("\"Hello world\"\n")
	fmt.Printf("%q\n", "Hello World")
}

// ---------------------------------------------------------
// EXERCISE: Print the Type
//
//  Print the type and value of 3 using fmt.Printf
//
// EXPECTED OUTPUT
//  Type of 3 is int
// ---------------------------------------------------------
func printType1() {

	n := 3

	fmt.Printf("Type of %d is %T\n", n, n)
}

// ---------------------------------------------------------
// EXERCISE: Print the Type #2
//
//  Print the type and value of 3.14 using fmt.Printf
//
// EXPECTED OUTPUT
//  Type of 3.14 is float64
// ---------------------------------------------------------
func printType2() {

	n := 3.14
	fmt.Printf("Type of %.2f is %T\n", n, n)
}

// ---------------------------------------------------------
// EXERCISE: Print the Type #3
//
//  Print the type and value of "hello" using fmt.Printf
//
// EXPECTED OUTPUT
// 	Type of hello is string
// ---------------------------------------------------------
func printType3() {

	s := "hello"
	fmt.Printf("Type of %q is %T\n", s, s)
}

// ---------------------------------------------------------
// EXERCISE: Print the Type #4
//  Print the type and value of true using fmt.Printf
//
// EXPECTED OUTPUT
//  Type of true is bool
// ---------------------------------------------------------
func printType4() {

	b := true
	fmt.Printf("Type of %t is %T\n", b, b)

}

// ---------------------------------------------------------
// EXERCISE: Print Your Fullname
//
//  1. Get your name and lastname from the command-line
//  2. Print them using Printf
//
// EXAMPLE INPUT
//  Inanc Gumus
//
// EXPECTED OUTPUT
//  Your name is Inanc and your lastname is Gumus.
// ---------------------------------------------------------
func printName2() {

	in := os.Args
	fmt.Printf("My first name is %s last name is %s\n", in[1], in[2])

}
