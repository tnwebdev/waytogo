/*
	Greeter App
*/

package main

import (
	"fmt"
	"os"
)

func main() {

	name := os.Args[1:]

	fmt.Println("Hello", name, "!")
	name1, age := "Chris", 2019
	fmt.Println("My name is", name1)
	fmt.Println("My age is", age)
	fmt.Println("BTW, you shall pass!")
}
