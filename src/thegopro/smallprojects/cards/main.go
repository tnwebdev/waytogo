package main

import "fmt"

// Old main
/*
func main() {
	cards := deck{"Ace of Diamonds", newCard()}
	cards = append(cards, "Six of Spades")

	This block has been refactor in deck.go under print() method
	for i, card := range cards {
		fmt.Println(i, card)
	}

	cards.print() // Call the print function from deck.go
}


func newCard() string {
	return "Five of Hearts"
}
*/

func main() {

	cards := newDeck()

	fmt.Println(cards.toString())

	separator()

	cards.saveToFile("my_cards.txt")

	cards1 := newDeckFromFile("my_cards.txt")
	cards1.print()

	separator()

	cards2 := newDeck()
	cards2.shuffle()
	cards2.print()

}

// func main() {
// 	mess := "Hi There!"
// 	temp := []byte(mess)
// 	fmt.Println(temp)
// }
