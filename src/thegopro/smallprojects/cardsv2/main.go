package main

import (
	"fmt"
)

type contactInfo struct {
	email   string
	zipCode int
}

type person struct {
	firstName string
	lastName  string
	contact   contactInfo
}

// Second main method
func main() {

	// call newPerson to make Jim
	jim := newPerson()

	// Change jim fistName to Jimmy
	jim.updateName("Jimmy", "Dude")
	jim.print()

	// Make new person
	chris := newPerson()

	// Change person firstName to Chris
	chris.updateName("Chris", "Nguyen")
	chris.print()
}

// Print person object
func (p person) print() {
	fmt.Printf("%+v\n", p)
}

// make new person
func newPerson() person {

	p := person{
		firstName: "Jim",
		lastName:  "Smith",
		contact: contactInfo{
			email:   "jim@email.com",
			zipCode: 95378,
		},
	}

	return p
}

// update name
func (p *person) updateName(newFirstName string, newLastName string) {
	(*p).firstName = newFirstName
	(*p).lastName = newLastName
}
