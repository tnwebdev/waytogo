// Exercice 1 Modiffy the echo program to also print os.Args[0], the name of the command that invoke it

// This echo1 program prints its command-line arguments on one line.package 002_echo1

package main

import (
	"fmt"
	"os"
	"strings"    // here we need a string package 
)

func main() {
	fmt.Println(strings.Join(os.Args[0:], " "))		// Print index 0 to the end of the array
}
