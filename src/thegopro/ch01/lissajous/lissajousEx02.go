/*******************************************************************************************

Lissajous gennerates the GIF animations of fandom Lissajous figures.package lissajous

*******************************************************************************************/

package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var palette = []color.Color{color.RGBA{65, 15, 80, 1}, color.RGBA{169, 41, 208, 1},
	color.RGBA{22, 49, 227, 1}}

const (
	firstColor  = 0 // first color in palette
	secondColor = 1 // next color in palette
	thirdColor  = 3 // next color in palette
)

func main() {

	lissajous(os.Stdout)

}

func lissajous(out io.Writer) {

	const (
		cycles  = 5    // number of complete x occillator revolutions
		res     = 0.01 // angular resolution
		size    = 300  // image canvas covers [-size..+size]
		nframes = 64   // number of naimation frames
		delay   = 8    // delay between frames in 10ms units

	)
	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase difference
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*size+.5), size+int(y*size+.5), secondColor)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) // NOTE: ignoring ecoding errors

}
