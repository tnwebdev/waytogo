// Echo3 prints its command-line arguments on one line

package main

import (
	"fmt"
	"os"
	"strings" // here we need a string package
)

func main() {
	arg := os.Args[1:]
	fmt.Println(strings.Join(arg, " "))
}
