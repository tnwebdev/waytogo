package main

import "fmt"

func main() {
	fmt.Println("hello, my name is Chris Nguyen!")
	fmt.Println("Go is interesting, it seem like I can quickly understand and enjoy coding in GO much faster then any other language I have learned before, like C, C++, Java, Perl, and JavaScript")
}
