package main

import "fmt"

var lightSwitchOn bool = false

func main() {

	printSwitchState()
	toggleSwitch()
	printSwitchState()
	toggleSwitch()
	printSwitchState()

}

func printSwitchState() {

	fmt.Println("The light is off:", lightSwitchOn)

}

func toggleSwitch() {

	lightSwitchOn = !lightSwitchOn

}
