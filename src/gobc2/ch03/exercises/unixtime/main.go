/*
	Playing around with Unix time
*/

package main

import (
	"fmt"
	"time"
)

func main() {
	totalMilliSeconds := time.Now().UnixNano() / int64(time.Microsecond)

	totalSeconds := totalMilliSeconds / 1000

	currentSecond := totalSeconds % 60

	totalMinutes := totalSeconds / 60

	currentMinute := totalMinutes % 60

	totalHours := totalMinutes / 60

	currentHour := totalHours % 24

	fmt.Printf("totalMilliSeconds = %d \n", totalMilliSeconds)
	fmt.Printf("totalSeconds = %d \n", totalSeconds)
	fmt.Printf("currentSecond = %d \n", currentSecond)
	fmt.Printf("totalMinutes = %d \n", totalMinutes)
	fmt.Printf("currentMinute = %d \n", currentMinute)
	fmt.Printf("totalHours = %d \n", totalHours)
	fmt.Printf("currentHour = %d \n", currentHour)
}
