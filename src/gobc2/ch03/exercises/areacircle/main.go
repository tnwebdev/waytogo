/*
	Calculate the Area of a circle
*/

package main

import "fmt"

func main() {

	r := 50
	fr := float64(r)
	areaCircleCalc(fr)

}

func inputValidation(i float64) bool {
	if i < 0 {
		return false
	}
	return true
}

func areaCircleCalc(r float64) float64 {
	pi := 3.13145
	var area float64
	pass := inputValidation(r)
	if !pass {
		fmt.Println("Please enter a positive value")
	} else {
		area = pi * (2 * r)
		fmt.Printf("The area of a circle with radius of %.2f is %.2f\n", r, area)
	}
	return area
}
