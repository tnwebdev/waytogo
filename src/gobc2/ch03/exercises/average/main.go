/*
	Calculate average
*/

package main

import "fmt"

func main() {
	num1 := 35 // declare and assign 35 to num1
	num2 := 25
	num3 := 65
	a := (num1 + num2 + num3) / 3                                         // Calculate average
	fmt.Printf("Average of %v, %v, and %v is %v \n", num1, num2, num3, a) // Print result
}
