/***************************************************************************************************
	Working with Bitwise
 ***************************************************************************************************/

package main

import "fmt"

func main() {

	var a uint8 = 5
	fmt.Printf("%8d %#8o %#8x \t %08b \n", a, a, a, a)

	a = a << 1
	fmt.Printf("%8d %#8o %#8x \t %08b \n", a, a, a, a)

	a = a >> 2
	fmt.Printf("%8d %#8o %#8x \t %08b \n", a, a, a, a)

	a = 4 | 2
	fmt.Printf("%8d %#8o %#8x \t %08b \n", a, a, a, a)

	a = 4 & 2
	fmt.Printf("%8d %#8o %#8x \t %08b \n", a, a, a, a)

	b := ^4
	fmt.Printf("%8d %#8o %#8x \t %08b \n", b, b, b, b)

	fmt.Println()
	base8octal()

}

func base8octal() {

	d := 125
	fmt.Printf("%8d %#8o %#8x \t %08b\n", d, d, d, d)

	c := 0175
	fmt.Printf("%8d %#8o %#8x \t %08b\n", c, c, c, c)

	e := 0x7d
	fmt.Printf("%8d %#8o %#8x \t %08b \n", e, e, e, e)
}
