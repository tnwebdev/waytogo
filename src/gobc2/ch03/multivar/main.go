// Defining Multiple Variables

package main

import "fmt"

func main() {
	var (
		a = 5
		b = 2
		c = 6
	)

	fmt.Println("(5 + 2) * 6 =", (a+b)*c)

	constVar()
}

// constant variable using const

func constVar() {
	const a, b = 10, 20
	var result int

	result = a * b
	fmt.Println(result)

	result = a + b
	fmt.Println(result)
}
