/**************************************************************************************************
	Working with NaN
*************************************************************************************************/

package main

import (
	"fmt"
	"math"
)

func main() {

	var a float64
	fmt.Println(a, -a, 1/a, -1/a, a/a)

	nan := math.NaN()
	fmt.Println(nan == nan, nan < nan, nan > nan)
}
