/**************************************************************************************************

	Variables: A variable is a storage location, with a specific type and an associated name.

**************************************************************************************************/

package main

import "fmt"

func main() {
	var x string = "Hello, World" // full syntax - assign veriable method

	var name string // another way assign variable
	name = "Chris"

	fmt.Println(x)
	fmt.Println(name)

	sep()
	vari01()

	sep()
	vari02()

	sep()
	vari03()

	sep()
	vari04()

	sep()
	vari05()
}

// Variable can change their value through the lifetime of a program
func vari01() {
	var x string
	x = "First" // assing "First" string fo x
	fmt.Println(x)
	x = "Second" // assign "second" to x, so x is now changed to "second"
	fmt.Println(x)
}

// We can even do this:
func vari02() {
	var x string
	x = "First "
	// x = x + "second"
	x += "second" // string concatenation
	fmt.Println(x)
}

// Separator
func sep() {
	fmt.Println("---------------------------------------------------------------------")
}

// using variable with equality sign
func vari03() {
	var x string = "Hello"
	var y string = "World"

	fmt.Println(x == y) // false

	x = "hello"
	y = "hello"

	fmt.Println(x == y) // truee
}

// Shorthand variable assignment
func vari04() {
	first := "Chris"
	last := "Nguyen"

	fmt.Println(first + " " + last)
}

// more on variable declaration
func vari05() {
	s1, s2 := "Hello, GoLang!", "Go is the most simple language in modern software development."
	var s3 = "Go is a good general purpose coding language,"
	var s4 string = "Go's syntax is compact and easy to parse"

	fmt.Println(s1 + " " + s2 + " " + s3 + " " + s4)
}
