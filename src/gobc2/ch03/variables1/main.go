package main

import "fmt"

func main() {
	sep("Block 1")
	var ui8 uint8 = 55 // Declare var ui8 of type uint8, assign value of 55

	const pi = 3.14159 // declare const pi assign value 3.14159 of type float
	// pi = 32			// changing pi's value is not allow due to variable type const

	ui8 /= 2 // 55 / 2 = 27

	fmt.Println(25, pi, ui8) // 25 3.14159 27

	sep("block 2")

	const (
		a = 2 // declare const a = 2
		b     // 2 is also assigned to b, and c
		c
		d = a * 10 // declare const a and assign 2 * 10 to d, same to e, and f
		e
		f
	)
	fmt.Println(a, b, c, d, e, f) // 2 2 2 20 20 20

	sep("iota-1") // Print separator for readability
	iota1()       // call iota1()

	sep("iota-2")
	iota2() // call iota2()
}

// iota
func iota1() {
	const ( // declare const
		a = iota + 1 // a = 1
		b            // b = 2
		c            // c = 3
		d = a * 20   // d = 20
		e            // e = 20
		f = iota     // f = 5
	)
	fmt.Println(a, b, c, d, e, f) // 1 2 3 20 20 5
}

// Bit shifting
func iota2() {
	const (
		a = iota + 1 // a is on bit 1
		b = a << 1   // b is on bit 2
		c = b << 1   // c is on bit 4
		d = c << 1   // d is on bit 8
		e = d << 1   // e is on bit 16
	)
	fmt.Println(a, b, c, d, e) // 1 2 4 8 16
}

// Just a separator for output readability
func sep(s string) string {
	fmt.Println(s, "---------------------------------------------------------------------")
	return s
}
