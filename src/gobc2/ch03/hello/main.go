package main

import "fmt"

var myName string = "Chris N." // variable at package level is accessible to all in the package

func main() {

	// var greeting string = "Hello, World!"	// Method one
	// var greeting string						// Declare variable greeting with 0 value
	// greeting = "Hello, world!"				// Assign veriable greeting with "Hello, World!"
	greeting := "Hello, World! " // Shorthand

	fmt.Println(greeting)
	fmt.Println("My name is ", myName)
}
