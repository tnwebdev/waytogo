// Find the smallest number of the array

package main

import "fmt"

func main() {

	x := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 83, 27,
		19, 97, 9, 17,
	}

	var n, smlist int
	for _, val := range x {
		if val > n {
			n = val
		} else {
			n = val
			smlist = n
		}
	}
	fmt.Println(smlist)
}
