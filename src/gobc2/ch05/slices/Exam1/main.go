package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3}
	slice2 := append(slice1, 4, 5)

	fmt.Println(slice1, slice2)

	// Let modify elements in slice2
	for i, v := range slice2 {
		slice2[i] = v + 2
	}

	// Print out the elements of slice1 and slice2
	fmt.Printf("slice1 : %#v\n", slice1)
	fmt.Printf("slice2 : %#v\n", slice2)

	fmt.Println("-----------------------")

	copySlice()
}

func copySlice() {
	slice1 := []int{1, 2, 3}
	slice2 := make([]int, 4)
	slice3 := make([]int, 2)
	copy(slice2, slice1)
	copy(slice3, slice1)
	fmt.Println(slice1, slice2, slice3)
}
