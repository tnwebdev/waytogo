// COPY | Copy take two arguments: dst and src. All of the entries in src are copied into dst
// overwriting whatever is there. If the lengths of the two slices are not the same, the small
// of the two will be used.

package main

import "fmt"

func main() {

	slice1 := []int{1, 2, 3}
	slice2 := make([]int, 5, 10)
	copy(slice2, slice1)

	fmt.Printf("slice1 %#v\n", slice1)
	fmt.Printf("slice2 %#v\n", slice2)

	// Append some more data to slice2
	slice2 = append(slice2, 4, 5, 6, 7, 8)
	fmt.Printf("slice2 %#v\n", slice2)
}
