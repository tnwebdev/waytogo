package main

import "fmt"

func main() {
	x := [5]float64{98, 93, 77, 82, 83}

	var total float64 = 0

	// Traditonal for loop
	// for i := 0; i < len(x); i++ {
	//	total += x[i]
	// }

	// Go loop
	for _, v := range x {
		total += v
	}

	// Print the output
	fmt.Printf("all score = %v\n", x) // total of all values from the array
	fmt.Printf("Total score = %v\n", total)
	fmt.Printf("Average = %v\n", total/float64(len(x))) // average of all value in the array
}
