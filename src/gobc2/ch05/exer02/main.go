// Write a program that find the smallest numbe in the list
// 48 96 68 86
// 57 83 62 70
// 37 34 83 27
// 19 79 09 17

package main

import "fmt"

func main() {

	num := []int{
		48, 96, 68, 86,
		57, 83, 62, 70,
		37, 34, 83, 27,
		19, 79, 9, 17,
	}

	smallest := num[0]

	for _, el := range num {
		if el < smallest {
			smallest = el
		}
	}
	fmt.Println(num)
	fmt.Printf("The smallest number of the array list is %v\n", smallest)
}
