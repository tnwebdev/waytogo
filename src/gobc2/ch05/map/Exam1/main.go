// MAPS
// A map is an unordered collection of key-value pairs (maps are also sometimes called associative arrays,
// hash tables, or dictionaries).
// Map are used to look up a value by its associated key.

package main

import "fmt"

func main() {

	x := make(map[string]int)

	x["key"] = 10
	fmt.Println(x["key"])

	// We can also create map with a key type of int
	y := make(map[int]int)
	y[1] = 10
	fmt.Println(y[1])

}
