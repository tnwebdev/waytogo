package main

import "fmt"

func main() {

	var nums = []int{23, 43, 29, 99, 84, 33, 22}

	fmt.Printf("The sum of %d is %d\n", nums, add(nums))
}

func add(n []int) int {

	sum := 0
	for _, v := range n {
		sum += v
	}
	return sum
}
