package main

import (
	"fmt"
)

func main() {

	x := 5
	zero(x)                           // without pointer
	fmt.Printf("x is still %v \n", x) // x is still 5
	zeroXp(&x)                        // with pointer
	fmt.Printf("x is now %v\n", x)    // x is now 0
}

func zero(x int) {
	x = 0
}

func zeroXp(xP *int) {
	*xP = 0
}
