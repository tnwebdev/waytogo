// Function Example

package main

import "fmt"

/*
func average(xs []float64) float64 {
	panic("Not Implemented")
}
*/

func main() {
	scoreList := []float64{98, 93, 77, 82, 83}

	fmt.Println(average(scoreList))
}

// Calculate average
func average(sl []float64) float64 {
	total := 0.0
	for _, val := range sl {
		total += val
	}
	return total / float64(len(sl))
}
