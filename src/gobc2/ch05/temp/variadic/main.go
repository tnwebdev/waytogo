// Variadic Functions Example

package main

import "fmt"

func main() {
	sum := add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10) // Passing multiple arguments
	sum1 := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	fmt.Println(sum)
	fmt.Println(add(sum1...)) // Passing a slice on int
}

// Add argument of type int
func add(args ...int) int {
	total := 0
	for _, val := range args {
		total += val
	}
	return total
}
