package main

import "fmt"

func main() {

	intExam01()
	sep()
	intExam02()
}

// Testing Integer and Floating data type

func intExam01() {
	x := 5 // declared and assigned type int
	y := 10
	a := 1.5 // declared and assigned type float64
	b := 1.0

	fmt.Printf("%v + %v = %v\t%T\n", x, y, x+y, x+y)
	fmt.Printf("%.2f + %.2f = %.2f\t%T\n", a, b, a+b, a+b)
}

func intExam02() {
	var i int
	fmt.Println("i =", i)

	j := 2
	fmt.Printf("%d - %T\n", j, j)

	var k1 uint8 = 20
	fmt.Printf("%d - %T\n", k1, k1)

	k1 = 255 // 255 is the highest value for 8bits
	// K1 = 256		// overflow, 256 is just larger than 8 bits
	// k1 = -2		// overflow
	fmt.Printf("%d - %T\n", k1, k1)

	var k2 uint16 = 50
	fmt.Printf("%d - %T\n", k2, k2)
	// k2 = k1 // error - type uint16 vus uint8

	k2 = uint16(k1) + 255 // Type conversion
	fmt.Printf("%d - %T\n", k2, k2)

	fmt.Println(uint64(456456 * 345663))

}

func sep() {
	fmt.Println("-------------------------------------------------------------------")
}
