/**************************************************************************************************
	Testing the fmt parameters
**************************************************************************************************/
package main

import "fmt"

var num1 uint8 = 255

func main() {

	fl1 := 5.55                               // Declare and assign
	fmt.Printf("%f - %T\n", fl1, fl1)         // %f = float; %T = type;
	fmt.Printf("%5.3f %.2f \n", fl1, 353.235) // Set number of digit to print at integer and fraction

	fmt.Println(fl1+12, fl1-12, fl1*2, fl1/2, 5%3) // fraction and integer  works without conversion

	fl1++            // Adding 1 to 5.55
	fmt.Println(fl1) // output 6.55

	fl1 += 10        // adding 10 to 6.55
	fmt.Println(fl1) // output 16.55

	fmt.Println(5/2, 5.0/2.0) // 2, 2.5
	sep()

	printFormat()
	sep()

	printFormat1()
}

func printFormat() {
	fmt.Printf("%d %o %x %b \n", byte('A'), byte('A'), byte('A'), byte('A'))

	fmt.Printf("%d %[1]o %[1]x %[1]b \n", byte('A'))

	fmt.Printf("%d %o %x %b \n", 'A', 'A', 'A', 'A')
}

func printFormat1() {
	var i uint8 = 55
	var f float32 = 235.678
	var b bool = true
	var msg string = "Hello"

	type myStringT string
	var myMsg myStringT = "Special message"

	fmt.Printf("%d %v %T \n", i, i, i)
	fmt.Printf("%5.1f %.4f %g %T \n", f, f, f, f)
	fmt.Printf("%t %v %T \n", b, b, b)
	fmt.Printf("%s %T \n", msg, msg)
	fmt.Printf("%v %T \n", myMsg, myMsg)
}

// Separator
func sep() {
	fmt.Println("---------------------------------------------------------------------")
}
