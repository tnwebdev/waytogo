package main

import "fmt"

// Testing string data type
func main() {
	fmt.Println(len("Hello, World")) // 12

	fmt.Println("Hello, World"[1]) // 101

	fmt.Println("Hello, " + "World!") // Hello, World!

	stringType1() // Call stringType1 function

}

func stringType1() {
	fmt.Println(len("Good Day")) // 8
	fmt.Println("Good Day"[5])   // D (58)
	fmt.Println()                // Blank line

	str := "Sunny Ocean" // declare a string

	fmt.Println(str[:5])  // Sunny (print from index 0 - 4)
	fmt.Println(str[5:])  //  Ocean (print from index 5 - end of string)
	fmt.Println(str[2:9]) // nny Oce
	fmt.Println(str[:])   // Sunny Ocean

	fmt.Println(str[:5] + str[5:]) // Sunny Ocean

	str += "Way to Go" // "Sunny Ocean" + "Way to Go"
	fmt.Println(str)   // Sunny OceanWay to Go
}
