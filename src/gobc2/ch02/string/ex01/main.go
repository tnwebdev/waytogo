package main

import "fmt"

func main() {

	// String within "Text"
	fmt.Println("This is a string inside the double quotes")

	// String inside back ticks
	fmt.Println(`Line 1, new line 
	
	Works in size backticks
	`)

	// String concatination
	fmt.Println("One " + "Tow " + "Three!")

	// Find length with of string
	str := "Tell me how many charactors in this string!"
	fmt.Printf("Tell me how many charactors in this string! %v\n", len(str))

	// Access a charater in the string
	abc := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	fmt.Printf("%v %v\n", abc, abc[2])

}
