/***************************************************************************************************
	Equality
***************************************************************************************************/

package main

import "fmt"

func main() {
	str := "Hello"
	str = "Hi, There!, what is your name?"

	fmt.Println(str == "Hello")
	fmt.Println(str != "Hello")

	var (
		name1 = "Chris"
		name2 = "Alan"
		name3 = "Brian"
	)

	fmt.Println(name1, name2, name3)
}
