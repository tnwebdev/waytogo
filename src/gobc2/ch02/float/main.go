package main

import "fmt"

// Testing Integer and Floating data type

func main() {
	fmt.Println("1 + 1 =", 1+1)
	fmt.Println("1 + 1 =", 1.0+1.0)
}
