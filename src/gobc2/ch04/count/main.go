package main

import "fmt"

func main() {

	// Method 1
	i := 1
	for i <= 10 {
		fmt.Println(i)
		i++
	}

	// Method 2
	for j := 1; j <= 10; j++ {
		fmt.Println(j)
	}
}
