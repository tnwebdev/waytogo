// Exercise 2: Write a program that from 1 - 100, prints out all the numbers evenly divisible by 3

package main

import "fmt"

func main() {
	i := 1
	for i <= 100 {
		if i%3 == 0 {
			fmt.Println(i, "Bizz")
		}
		if i%5 == 0 {
			fmt.Println(i, "Buzz")
		}
		if i%3 == 0 && i%5 == 0 {
			fmt.Println(i, "Bizz, Buzz")
		}
		i++
	}
}
