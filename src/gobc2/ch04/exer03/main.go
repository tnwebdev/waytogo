/*
Hands-on exercise #3
Create a for loop using this syntax
for condition { }
Have it print out the years you have been alive.
*/
package main

import "fmt"

func main() {
	var i, y = 1970, 2020
	age := y - i
	for {
		fmt.Printf("%v\n", i)
		if i == y {
			fmt.Printf("You have been alive for %v years\n", age)
			break
		}
		i++
	}
}
