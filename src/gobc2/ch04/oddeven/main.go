package main

import "fmt"

func main() {
	var num int

	for i := 1; i <= 50; i++ {
		num++
		if num%2 == 0 {
			fmt.Println(num, " is even")
		} else {
			fmt.Println(num, " is odd")
		}
	}
}
