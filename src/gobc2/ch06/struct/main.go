package main

import (
	"fmt"
	"math"
)

// Circle struct

type Circle struct {
	x, y, r float64
}

func main() {

	c := Circle{x: 0, y: 0, r: 5}

	fmt.Println(c.x, c.y, c.r)
	c.x = 10
	c.y = 5

	fmt.Println(c.x, c.y, c.r)

	fmt.Println(circleArea(&c))
}

func circleArea(c *Circle) float64 {
	return math.Pi * c.r * c.r
}
