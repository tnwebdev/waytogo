package main

import (
	"fmt"
	"math"
)

type Circle struct {
	x, y, r float64
}

type Rectangle struct {
	x1, y1, x2, y2 float64
}

func main() {

	c := Circle{x: 0, y: 0, r: 5}
	r := Rectangle{x1: 0, y1: 0, x2: 10, y2: 10}

	fmt.Println(c.area())
	fmt.Println(r.area())
}

func distance(x1, y1, x2, y2 float64) float64 {
	a := x2 - x1
	b := y2 - y1
	// fmt.Println(a, b)
	return math.Sqrt(a*a + b*b)
}

func (c *Circle) area() float64 {
	return math.Pi * c.r * c.r
}

func (r *Rectangle) area() float64 {
	l := distance(r.x1, r.y1, r.x1, r.y2)
	w := distance(r.x1, r.y1, r.x2, r.y1)
	fmt.Println(l, w)
	return l * w
}
