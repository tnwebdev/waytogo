# WAY TO GO  

This is a learning project to use **GO Programming Language** in full-stack web development work.  

The source code and the **summarized** text written in this project's document are the collective knowledge learned from **[Golang.org](https://golang.org/)** documentation and the book title **[The Go Programming Language](http://www.gopl.io/)** by **Alan A. A. Donovan** - Google Inc. && **Brian W. Kernighan** - Princeton University.

The goal of this project is to thoroughly learn Go and use Go as primary programming language for all Fullstack Web Development work going forward.

## Testing out Go - My first little Go program  

[See the code here](src/001_hello/hello.go)  

```Go
package main

import "fmt"

func main () {
   fmt.Println("Hello, world!")
}
```

# Introduction to **GO**

## Packages

Go code is organized into **packages**, a package consists of one or more **.go** source files in a single directory that define **what the package does**. each source file begins with a package declaration.  

- The first line in the **hello.go** source file is package **main** declaration
- The **import** declaration must follow the **package** declaration, in this case **import "fmt"** package which contains the function **Println** needed to print out the string specified in the function argument.  

- Follow package and import declaration is the program body, consist of function declation using the **func** keyword.  
- Go does not require **semicolons** at the end of the statements or declarations, except where two or more statements appear on the same line.  
- Newlines following certain tokens are converted into semicolons, so where newlines are placed matters to the proper **parsing** of the Go code.  

## Command-Line Arguments  

Most programs process some input to produce some output: that is very much the definition of computing.  
Go **os** package provides functions and other values for dealing with the operating system in a platform-independent fashion. Command-line arguments are vailable to program in a variable name **Args** is part of the **os** package, therefor the name out side the **os** package is **os.Args**.  

[echo1.go code example](src/002_echo1/echo1.go)

Echo1 program prints its command-line arguments on a single line  

```Go
package main

import (
   "fmt"
   "os"
)

func main() {
   var s, space string   // declare veriable s, and sep of type string to and empty value
   for i := 1; i < len(os.Args); i++ {
      s += space + os.Args[i]   // concatenation of command-line arguments
      space = " "   // Add space after each argument
   }
   fmt.Println(s)
}
```  

## For loop - The only Loop Statement in GO  

For loop is the only loop statement in **Go** it has a number of forms which will be illustrated in following code examples:

```Go
for initialization; condition; post {  

   // Zero or more statements  

}  
```

Parentheses are never used around the three components of the **for** loop. The braces are mondatory and the opening brace must be on the same line with the **post** statement.  

The inintialization and post are optional and can be omitted, if there is no initialization and no post, the memicolons may also be omitted.  

The traditional ***while*** loop could be done using **Go** for loop as follow:

```Go
// A traditional "while" loop
for condition {
   // Statement ...
}
```  

If the condition is ommitted entirely in any of those forms, a traditional infinite loop could be done in **Go** for loop.

```Go
// a traditional infinite loop
for {
   // statement ...
}
```

This form of loop may be terminated in break or return statement.

### echo2 example - A Different Way to Use the For Loop in **Go**

Another form of the for loop iterates over a range of values from data type like a string or a slice. See the second version of echo:  

```Go
// Echo2 prints its command-line arguments.
package main

import (
   "fmt"
   "os"
)

func main() {
   s, space := "", ""   // short hand declaration of variable s and space with empty string value
   for _, arg := range os.Args[1:] {  // ignore the index, only assign the value to arg variable
      s += space + arg   // cancatenation as normal
      space = " "
   }
   fmt.Println(s)   // out put what store in s
}

```  

## Variable in **Go**

```Go
str := ""

var str string  

var str = ""  

var str string = ""

```

These are the equivalent ways of declaring data type string to a variable

```Go
str := ""   // the most compact form, but it shoud only be used within a function scope, not the package-level variable.  

var str string   // best use when one needed to assign the default value of zero which is ""  

var str = ""    // this form is rarely use except when needed to declare multiple variables  

var str string = ""    // Explicit about verable's type, which is redundant when it is the same as the initual value  
```

## Join Function in Strings Package

See echo3.go for a more efficient solution when it come garbage-collection

```Go
// Echo3 prints its command-line arguments on one line

package main

import (
   "fmt"
   "os"
   "strings"    // here we need a string package
)

func main() {
   fmt.Println(strings.Join(os.Args[1:], " "))
}
```

## Time to Code  

### Exercise 1

Exercice 1 - Modiffy the echo program to also print os.Args[0], the name of the command that invoke it

Solution:

```Go
// This echo1 program prints its command-line arguments on one line.package 002_echo1

package main

import (
   "fmt"
   "os"
   "strings"    // here we need a string package
)

func main() {
   fmt.Println(strings.Join(os.Args[0:], " "))  // Print index 0 to the end of the array
}
```

### Exercise 2

Exercise 2 - Modify the echo program to print the index and value of each of its arguments, one per line.

Solution:

```Go
// This echo1 program prints its command-line arguments on one line.package 002_echo1

package main

import (
   "fmt"
   "os"
)

func main() {
   var s, space string     // declare veriable s, and sep of type string
   for i := 1; i < len(os.Args); i++ {
      s += space + os.Args[i]    // concatenation of command-line arguments
      space = " "   // Add space after each argument
      fmt.Println(i)
      fmt.Println(os.Args[i])
   }
   fmt.Println(s)
}  
```

## Finding Duplicate Lines

An introduction into some general purpose command line utility tools are file copying, searching, sorting, counting. Here are the three variants of a program called dup inspied by Unix **uniq** command.

dup1.go 

```go
/***********************************************************************************************
 Dup1 prints the text of each line that appears more than once in the standard input, preceded
 by its count
***********************************************************************************************/
package main

import (
   "bufio"
   "fmt"
   "os"
)

func main() {
   counts := make(map[string]int)
   input := bufio.NewScanner(os.Stdin)
   
   for input.Scan() {
      counts[input.Text()]++
   }
   
   // Note: ignoring potential errors from input.Err()
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

```

dup2.go

```go
/*********************************************************************************************

	Dup2 print s the count and text of the lines that appear more than once in the input.
	It reads from stdin or from a list of named files.

**********************************************************************************************/
package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	counts := make(map[string]int)
	files := os.Args[1:]

	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n:", err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

func countLines(f *os.File, counts map[string]int) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[input.Text()]++
	}
	// NOTE: ignoring potential errors from input.Err()
}
```

dup3.go

```go

/*******************************************************************************************************

	Dup3 is a simplified version, it only reads named files, not the standard input, since ReadFile
	requires a file name arguments.package dup3

*******************************************************************************************************/
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	counts := make(map[string]int)

	for _, filename := range os.Args[1:] {
		data, err := ioutil.ReadFile(filename)
		if err != nil {
			fmt.Fprintf(os.Stderr, "dup3: %v\n", err)
			continue
		}
		for _, line := range strings.Split(string(data), "\n") {
			counts[line]++
		}
	}
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\t%s\n", n, line)
		}
	}
}

```

## Animated GIFs

A domonstration of Go standard image packages in this section create a sequence of bit-mapped images and then encoded the sequence as GIF animation.

There are several new constructs in this code, including **const** declarations, **struct types**, and **composite literals**. 

lissajous.go

```go

/*******************************************************************************************

Lissajous gennerates the GIF animations of fandom Lissajous figures.package lissajous

*******************************************************************************************/

package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"math/rand"
	"os"
)

var palette = []color.Color{color.White, color.Black}

const (
	whiteIndex = 0 // first color in palette
	blackIndex = 1 // next color in palette

)

func main() {

	lissajous(os.Stdout)

}

func lissajous(out io.Writer) {

	const (
		cycles  = 5    // number of complete x occillator revolutions
		res     = 0.01 // angular resolution
		size    = 100  // image canvas covers [-size..+size]
		nframes = 64   // number of naimation frames
		delay   = 8    // delay between frames in 10ms units

	)
	freq := rand.Float64() * 3.0 // relative frequency of y oscillator
	anim := gif.GIF{LoopCount: nframes}
	phase := 0.0 // phase difference
	for i := 0; i < nframes; i++ {
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < cycles*2*math.Pi; t += res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(size+int(x*size+0.5), size+int(y*size+0.5), blackIndex)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay, delay)
		anim.Image = append(anim.Image, img)
	}
	gif.EncodeAll(out, &anim) // NOTE: ignoring ecoding errors
}

```

### Excercise 01

Change the Lissajous program's color palette to green on black, for added authenticity.

[Solution](src/ch01/lissajous/lissajousEx01.go)

![exercise01.gif](src/ch01/lissajous/exercise01.gif)

### Excercise 02

Modify the Lissajous program to produce images in multiple colors by adding more values to palette and then displaying them by changing the third argument of SetColorIndex in some interesting way.

[Solution](src/ch01/lissajous/lissajousEx02.go)

![exercise01.gif](src/ch01/lissajous/exercise02.gif)

## Fetching a URL

Applications with access to information from the internet is the must in software development today. Go provides a collection of packages, grouped under **net**, that make it easy to send and receive information through the Internet, make low-level network connections, and set up servers, for which Go's concurrency features.

```go

/************************************************************************************************

	Fetch prints the content found at a URL.package fetchurl

************************************************************************************************/

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {

	for _, url := range os.Args[1:] {
		resp, err := http.Get(url)
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: %v\n", err)
			os.Exit(1)
		}
		b, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			fmt.Fprintf(os.Stderr, "fetch: reading %s: %v\n", url, err)
			os.Exit(1)
		}
		fmt.Printf("%s", b)
	}

}

```

## Fetching URLs Concurrently

One of the big aspects of Go is its support for concurrent programming. This is the simple example of Go main's concurrency code, goroutines and channels.

### Introduction to fetch URL with concurrency

**fetchall.go**

```go

/**************************************************************************************************

	Fetchall fetches URLs in parallel and reports their times and sizes.package fetchall

**************************************************************************************************/

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func main() {

	start := time.Now()
	ch := make(chan string)		// make creates a channel of strings 
	for _, url := range os.Args[1:] {	// for each cli arguments Go fetch 
		go fetch(url, ch) // start a goroutine
	}
	for range os.Args[1:] {
		fmt.Println(<-ch) // receive from channel ch
	}
	fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())
}

func fetch(url string, ch chan<- string) {
	start := time.Now()
	resp, err := http.Get(url)
	if err != nil {
		ch <- fmt.Sprint(err) // send to channel ch
		return
	}

	nbytes, err := io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close() // don't leak resources
	if err != nil {
		ch <- fmt.Sprintf("While reading %s: %v", url, err)
		return
	}
	secs := time.Since(start).Seconds()
	ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
}

```

## A Web Server

Creating web server in Go is simple and as Go's libraries provides all the tools that makes it easy to write web server that responds to cleint requests.

### Introduction to web server

**server1.go**

To start the server in the background: 

$ go run main.go &

```go

// Serverr1 is a minimal "echo" server.package main

package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", handler) // each request calls handler
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

// handler echoes the Path componet of the requested URL.
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

```

**server2.go**

This version does the same echo but also counts the number of requests; a request to the URL / count returns the count so far, excluding / count request themsels:

```go

// Server2  is a minimal "echo" and counter server.package main

package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var mu sync.Mutex
var count int

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/count", counter)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

// Handler echoes the Path component of the requested URl.
func handler(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	count++
	mu.Unlock()
	fmt.Fprintf(w, "URL.Path = %q\n", r.URL.Path)
}

// Counter echoes the number of calls so far.
func counter(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	fmt.Fprintf(w, "Count %d\n", count)
	mu.Unlock()
}

```

As a recher example, the handler function can report on the headers and form data that it receives making the server useful for inspecting and debugging requests:

**server3.go**

```go

// Handler echoes the HTTP request.package main
package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var mu sync.Mutex
var count int

func main() {
	http.HandleFunc("/", handler)
	http.HandleFunc("/count", counter)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "%s %s %s\n", r.Method, r.URL, r.Proto)

	for k, v := range r.Header {
		fmt.Fprintf(w, "Header[%q] = %q\n", k, v)
	}
	fmt.Fprintf(w, "Host = %q\n", r.Host)
	fmt.Fprintf(w, "RemoteAddr = %q\n", r.RemoteAddr)
	if err := r.ParseForm(); err != nil {
		log.Print(err)
	}
	for k, v := range r.Form {
		fmt.Fprintf(w, "Form[%q] = %q\n", k, v)
	}
}

// Counter echoes the number of calls so far.
func counter(w http.ResponseWriter, r *http.Request) {
	mu.Lock()
	fmt.Fprintf(w, "Count %d\n", count)
	mu.Unlock()
}


```


